FROM alpine:latest

RUN mkdir /work
COPY ./src/foo.sh /work/
RUN chmod +x /work/foo.sh

CMD ["/work/foo.sh"]

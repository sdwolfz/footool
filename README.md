# FooTool

It does almost nothing!

> **NOTE**: This project is feature complete!

## Docker

Build

```sh
docker build -t footool "$PWD"
```

Run

```sh
docker run --rm -it footool
```

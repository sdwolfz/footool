#!/usr/bin/env sh

set -ex

. ./scripts/download.sh

download_from_git                          \
  footool                                  \
  master                                   \
  451eb493757c22500ef020195828fef51370f2b6 \
  https://gitlab.com/sdwolfz/footool.git

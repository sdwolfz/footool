#!/usr/bin/env sh
set -ex

APPDIR="$PWD"/lib/footool

cd vendor/footool

./autogen.sh
./configure \
 --prefix="$APPDIR" \
 --enable-bar=yes

make -j`nproc`
make install -j`nproc`

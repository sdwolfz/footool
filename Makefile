.DEFAULT_GOAL = making

# ------------------------------------------------------------------------------
# Compilation

.PHONY: making
making:
	@echo "Making..."

.PHONY: install
install:
	@echo "Installing..."

# ------------------------------------------------------------------------------
# Package Management

.PHONY: all
all: download clean build

# ------------------------------------------------------------------------------
# Build

.PHONY: build
build: \
	build/footool

.PHONY: build/footool
build/footool:
	@sh scripts/build/footool.sh

# ------------------------------------------------------------------------------
# Clean

.PHONY: clean
clean: \
	clean/footool

.PHONY: clean/footool
clean/footool:
	@rm -f lib/footool

# ------------------------------------------------------------------------------
# Download

.PHONY: download
download: \
	download/footool

.PHONY: download/footool
download/footool:
	@sh scripts/download/footool.sh
